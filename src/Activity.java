/*
 * Klasa odpowiadająca jednemu wpisowi w historii aktywności dotyczącej błędu
 */
public class Activity {
	
	/*
	 * Nazwa użytkownika 
	 */
	private final String who;
	/*
	 * Nazwa zmienionego pola
	 */
	private final String what;
	/*
	 * Stara wartość zmienionego pola
	 */
	private final String removed;
	/*
	 * Nowa wartość zmienionego pola
	 */
	private final String added;
	
	public Activity(String who, String what, String removed, String added) {
		this.who = who;
		this.what = what;
		this.removed = removed;
		this.added = added;
	}
	
	String getWho() {
		return who;
	}
	
	String getWhat() {
		return what;
	}
	
	String getRemoved() {
		return removed;
	}
	
	String getAdded() {
		return added;
	}
	
}
