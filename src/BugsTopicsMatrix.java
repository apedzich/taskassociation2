/*
 * Klasa odpowiadaj�ca macierzy wi���cej kategori� z b��dem
 */
public class BugsTopicsMatrix {

	/*
	 * Liczba b��d�w
	 */
	final private int bugsNum;
	/*
	 * Liczba kategorii
	 */
	final private int topicsNum;
	/*
	 * Macierz
	 */
	private int[][] matrix;
		
		BugsTopicsMatrix(int bugsNum, int topicsNum) {
			this.bugsNum = bugsNum;
			this.topicsNum = topicsNum;
			this.matrix = new int[bugsNum][topicsNum];
		}
		
		public int getBugsNum() {
			return bugsNum;
		}
		
		public int getTopicsNum() {
			return topicsNum;
		}
		
		public int getFieldValue(int bugNr, int topicNr) {
			return matrix[bugNr][topicNr];
		}
		
		public void setFieldValue(int bugNr, int topicNr, int value) {
			matrix[bugNr][topicNr] = value;
		}
		
		public void increaseFieldValue(int bugNr, int topicNr, int inc) {
			matrix[bugNr][topicNr] += inc;
		}
}
