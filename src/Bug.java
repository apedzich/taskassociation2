import java.util.ArrayList;

/*
 * Klasa odpowiadająca raportowi o błędzie w repozytorium
 */
public class Bug {
	
	/*
	 * Id błędu
	 */
	private final int id;
	/*
	 * Użytkownik, który przypisał błąd do programisty
	 */
	private final User assigner;
	/*
	 * Użytkownik, który naprawił błąd
	 */
	private final User solver;
	/*
	 * Użytkownik, który sprawdził czy błąd został poprawnie naprawiony
	 */
	private final User reviewer;
	/*
	 * Słowa znaczące (opisu nie trzymamy)
	 */
	private final ArrayList<String> terms;
    
    public Bug(int id, User solver, User assigner, User reviewer, ArrayList<String> terms) {
    	this.id = id;
    	this.solver = solver;
    	this.assigner = assigner;
    	this.reviewer = reviewer;
    	this.terms = terms;
    }
    
    int getId() {
    	return id;
    }
    
    User getSolver() {
    	return solver;
    }
    
    User getAssigner() {
    	return assigner;
    }
    
    User getReviewer() {
    	return reviewer;
    }
    
    ArrayList<String> getTerms() {
    	return terms;
    }
    
    /*
     * Wypisuje termy powiazane z danym błędem
     */
    String getTermsToString() {
    	String str = "";
    	int cond = 0;
    	for (String term: terms) {
    		if (cond != 0)
    			str += " ";
    		else 
    			cond = 1;
    		str += term;
    	}
    	return str;
    }

}

