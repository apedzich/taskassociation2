import java.util.ArrayList;

/*
 * Klasa odpowiadaj�ca kategorii w modelu 
 */
public class Topic {

	/*
	 * Id kategorii
	 */
	private final int id;
	/*
	 * S�owa z danej kategorii (z wagami)
	 */
	private final ArrayList<Pair<String, Double>> words;
	
	Topic(int id, ArrayList<Pair<String, Double>> words) {
		this.id = id;
		this.words = words;
	}
	
	int getId() {
		return id;
	}
	
	ArrayList<Pair<String, Double>> getWords() {
		return words;
	}
	
	/*
	 * Wypisuje kategori� w postaci: ID S�OWO1 WAGA1 S�OWO2 WAGA2...
	 */
	@Override
    public String toString() {
    	String str = "";
    	str = str + id + " ";
    	for (Pair<String, Double> word:  words) {
    		str = str + word.getFirst() + " ";
    		str = str + word.getSecond() + " ";
    	}
    	return str;
    }
	
}
