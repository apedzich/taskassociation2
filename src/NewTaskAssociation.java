import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.InstanceList;

/*
 * Główna klasa
 */
class NewTaskAssociation {

	public static void main(String[] args) {
		
		//BugRepository r_kde = new BugRepository("https://bugs.kde.org");
		
		//Bug b = r_kde.getBug(10000);
    
		BugRepository r = new BugRepository("https://bugzilla.gnome.org");

		//Bug bug = r.getBug(77386);
		//System.out.println(bug.getTerms().size());
		
		//System.out.println(bug.getId() + " " + (bug.getAssigner() == null ? "NULL" : bug.getAssigner().toString()) + 
		//" " + (bug.getSolver() == null ? "NULL" : bug.getSolver().toString()) + 
		//" " + (bug.getReviewer() == null ? "NULL" : bug.getReviewer().toString()) + " " + bug.getTermsToString() + "\n");
		
		/*System.out.println(bu.getId());
		System.out.println(bu.getDesc());
		if (bu.getAssigner() != null) {
			System.out.println("Assigner");
			System.out.println(bu.getAssigner().getName());
		}
		if (bu.getSolver() != null) {
			System.out.println("Solver");
			System.out.println(bu.getSolver().getName());
		}
		if (bu.getReviewer() != null) {
			System.out.println("Reviewer");
			System.out.println(bu.getReviewer().getName());
		}*/
		
		ArrayList<Bug> solvedBugs = r.getSolvedBugsFromFile("bugs.txt");
		
		
		//ArrayList<Bug> solvedBugs = r.getSolvedBugs("verified", "bugs.txt", "time_table.txt");
		
		ArrayList<User> users = r.getUsers(solvedBugs, "users.txt");
		
		ArrayList<Integer> counters = r.getActionsCounter(solvedBugs, "counters.txt");
		
		ArrayList<Topic> topicsList = r.getTopicsFromFile("topics.txt");
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		
		//r.makeFileWithTerms(solvedBugs, "terms.txt");
		//InstanceList instances = r.generateInstances();
		int topicsNum = 100;
		//ParallelTopicModel model = r.generateTopicModel("terms.txt", topicsNum, instances);
		//ArrayList<Topic> topicsList = r.getTopicsFromFile("topics.txt");
		
		BugsTopicsMatrix mat = r.generateBugsTopicsMatrix(solvedBugs, topicsList, "matrix1.txt");
		
		//ArrayList<Integer> significiant = r.getSignificiantTopicNumbers(mat, 20, "significiant.txt");
		
		TopicsUsersMatrix mat2 = r.generateTopicsUsersMatrix(mat, topicsNum, users, solvedBugs, counters.get(0), counters.get(1), counters.get(2), "matrix2.txt");
			
		ArrayList<Integer> testSet = new ArrayList<Integer>();
		testSet.add(77350);
		testSet.add(77530);
		testSet.add(77572);
		testSet.add(77917);
		testSet.add(77945);
		testSet.add(77949);
		testSet.add(78067);
		testSet.add(78310);
		testSet.add(78310);
		testSet.add(78358);
		testSet.add(78536);
		
		for (int i : testSet) {
			ArrayList<User> userForBug = r.generateUsersListForBug(mat2, 30, users, topicsList, 20, i, "users" + i + ".txt");
			System.out.println("W " + i);
		}
		
		date = new Date();
		System.out.println(dateFormat.format(date));
		
		

		//InstanceList instances = r.generateInstances();
		
		//System.out.println("TUTAJ1");
		
		//int topicsNum = 100;
		//ParallelTopicModel model = r.generateTopicModel("desc.txt", topicsNum, instances);
		
		//System.out.println("TUTAJ2");
		
		//ArrayList<Topic> topicsList = r.getTopicsFromTopicsModel(model, instances, topicsNum, "topics.txt");
		
		//System.out.println(topicsList.get(0));
  
		//System.out.println("TUTAJ3");
		
		//RepositoryTreatment r2 = new RepositoryTreatment("https://bugzilla.kernel.org");
 
  
		//BugData bu2 = r2.getBugData(42950);
		
		/*System.out.println(bu2.getId());
		System.out.println(bu2.getDesc());
		if (bu2.getAssigner() != null) {
			System.out.println("Assigner");
			System.out.println(bu2.getAssigner().getName());
		}
		if (bu2.getSolver() != null) {
			System.out.println("Solver");
			System.out.println(bu2.getSolver().getName());
		}
		if (bu2.getReviewer() != null) {
			System.out.println("Reviewer");
			System.out.println(bu2.getReviewer().getName());
		}*/
		
		//r2.getSolvedBugs("closed");
  
	}

}