import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;

import java.net.MalformedURLException;
import java.net.URL;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.CharSequenceLowercase;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.pipe.TokenSequenceRemoveStopwords;
import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.InstanceList;

/*
 * Klasa odpowiadająca repozytorium Bugzilli
 */
public class BugRepository {

	/*
	 * Adres repozytorium
	 */
	private final String url;

	/*
	 * Dodatkowe stop słowa, na razie hardcodowane
	 */
	final CharArraySet stopSet = new CharArraySet(
			Arrays.asList("a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in", "into", "is", "it",
					"no", "not", "of", "on", "or", "such", "that", "the", "their", "then", "there", "these", "they",
					"this", "to", "was", "will", "with", "gnome", "i", "you", "me", "we", "us", "ve", "hi", "bye"),
			false);

	public BugRepository(String url) {
		this.url = url;
	}

	String getUrl() {
		return url;
	}

	/*
	 * Funkcja pozwalająca na czytanie zawartosci strony bez sprawdzania
	 * certyfikatów
	 */
	private void allowWithoutCheckingCerts() {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
					throws CertificateException {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
					throws CertificateException {
			}
		} };
		try {
			SSLContext sc;
			sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (NoSuchAlgorithmException e) {
			System.err.println("NoSuchAlgorithException happened while working with certificates.\n");
			e.printStackTrace();
		} catch (KeyManagementException e) {
			System.err.println("KeyManagementException happened while working with certificates.\n");
			e.printStackTrace();
		}
	}

	/*
	 * Funkcja zwracająca konkretne pole z głównej strony raportu o błędzie
	 * bugId - id błędu fieldName - nazwa pola, które chcemy otrzymać err -
	 * końcówka komunikatu na wypadek wystąpienia wyjątku
	 */
	public String getFieldValue(int bugId, String fieldName, String err) {
		String fieldValue = "";
		try {
			// Czytanie strony bez sprawdzania komunikatów
			allowWithoutCheckingCerts();
			String descUrl = url + "/show_bug.cgi?id=" + bugId;
			URL realUrl = new URL(descUrl);

			// Sczytywanie zawartości strony na zmienną
			BufferedReader in = new BufferedReader(new InputStreamReader(realUrl.openStream()));
			String inputLine;
			String result = "";
			while ((inputLine = in.readLine()) != null)
				result = result + inputLine + "\n";
			in.close();

			Document doc = Jsoup.parse(result); // Sparsowanie html
			Element elem = doc.select(fieldName).first(); // Wyszukanie
															// pierwszego tagu
															// pasującego do
															// patternu
			if (elem != null)
				fieldValue = Jsoup.parse(elem.toString()).text(); // Usunięcie
																	// tagów
																	// html
		} catch (MalformedURLException e) {
			System.err.println("MalformedURLException happened" + err);
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IOException happened" + err);
			e.printStackTrace();
		}
		return fieldValue;
	}

	/*
	 * Funkcja zwracająca opis błędu
	 */
	public String getBugDescription(int bugId) {
		return getFieldValue(bugId, ".bz_comment_text", " while getting description of bug " + bugId + ".\n");
	}

	/*
	 * Funkcja zwracająca status błędu
	 */
	public String getBugStatus(int bugId) {
		return getFieldValue(bugId, "#static_bug_status", " while getting status of bug " + bugId + ".\n");
	}

	/*
	 * Funkcja zwracająca produkt, którego dotyczy błąd
	 */
	public String getBugProduct(int bugId) {
		return getFieldValue(bugId, "#field_container_product", " while getting product of bug " + bugId + ".\n");
	}

	/*
	 * Funkcja zwracająca komponent, którego dotyczy błąd
	 */
	public String getBugComponent(int bugId) {
		return getFieldValue(bugId, "#field_container_component", " while getting product of bug " + bugId + ".\n");
	}

	/*
	 * Funkcja zwracająca użytkownika, przypisanego do błędu (według strony
	 * głównej błędu)
	 */
	public String getBugAssignee(int bugId) {
		return getFieldValue(bugId, ".fn", " while getting assigner of bug " + bugId + ".\n");
	}

	/*
	 * Funkcja zwracająca aktywność dotyczącą błędu
	 */
	public ArrayList<Activity> getBugActivity(int bugId) {
		ArrayList<Activity> activityList = new ArrayList<Activity>();

		try {
			allowWithoutCheckingCerts();
			String descUrl = url + "/show_activity.cgi?id=" + bugId;
			URL realUrl = new URL(descUrl);

			BufferedReader in = new BufferedReader(new InputStreamReader(realUrl.openStream()));
			String inputLine;
			String result = "";

			while ((inputLine = in.readLine()) != null)
				result = result + inputLine + "\n";
			in.close();
			Document doc = Jsoup.parse(result); // Sparsowanie html
			Elements elems = doc.select("td"); // Wyszukanie wartości w polach
												// tablicy

			if (elems.size() < 5) // Być może strona z aktywności błędu jest
									// pusta
				return activityList;

			elems.remove(0);
			elems.remove(0);
			int i = 0, j = 0, z = 0;
			int rowSpanWho = 0;
			int rowSpanWhen = 0;
			String tmp = "", who = "", what = "", removed = "", added = "";
			// Czytanie z tablicy
			for (Element elem : elems) {
				if (i == 0 && rowSpanWho > 1) {
					i = (i + 1) % 5;
					rowSpanWho--;
				}
				if (i == 1 && rowSpanWhen > 1) {
					i = (i + 1) % 5;
					rowSpanWhen--;
				}
				if (i == 0) { // Użytkownik
					tmp = elem.toString();
					j = tmp.indexOf("rowspan=");
					z = tmp.indexOf("\"", j + 9);
					rowSpanWho = Integer.parseInt(tmp.substring(j + 9, z));
					who = elem.text();
				} else if (i == 1) { // Czas
					tmp = elem.toString();
					j = tmp.indexOf("rowspan=");
					z = tmp.indexOf("\"", j + 9);
					rowSpanWhen = Integer.parseInt(tmp.substring(j + 9, z));
				} else if (i == 2) { // Nazwa pola
					what = elem.text();
				} else if (i == 3) { // Stara wartość pola
					removed = elem.text();
				} else { // Nowa wartość pola
					added = elem.text();
					activityList.add(new Activity(who, what, removed, added));
				}
				i = (i + 1) % 5;
			}
		} catch (MalformedURLException e) {
			System.err.println("MalformedURLException happened while getting activity of bug " + bugId + ".\n");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IOException happened while getting activity of bug " + bugId + ".\n");
			e.printStackTrace();
		}
		return activityList;
	}

	/*
	 * Funkcja zwracająca użytkowników powiązanych z błędem activityList - lista
	 * aktywności powiązanych z błędem
	 */
	public ArrayList<Pair<User, String>> getDevelopersConnectedToBug(ArrayList<Activity> activityList) {
		Boolean assigner_chosen, solver_chosen, reviewer_chosen;
		assigner_chosen = false;
		solver_chosen = false;
		reviewer_chosen = false;
		ArrayList<Pair<User, String>> developerList = new ArrayList<Pair<User, String>>();
		int cond = 0;
		Collections.reverse(activityList);

		for (Activity activity : activityList) {
			if (cond == 3) // Wszytkie role przypisane
				break;
			if (activity.getWhat().equalsIgnoreCase("assignee")) {
				if (!assigner_chosen) {
					developerList.add(new Pair<User, String>(new User(activity.getWho()), "assigner"));
					cond++;
					assigner_chosen = true;
				}
			} else if (activity.getWhat().equalsIgnoreCase("status")) {
				if (activity.getAdded().equalsIgnoreCase("assigned")) {
					if (!assigner_chosen) {
						developerList.add(new Pair<User, String>(new User(activity.getWho()), "assigner"));
						cond++;
						assigner_chosen = true;
					}
				} else if (activity.getAdded().equalsIgnoreCase("closed")
						|| activity.getAdded().equalsIgnoreCase("resolved")) {
					if (!solver_chosen) {
						developerList.add(new Pair<User, String>(new User(activity.getWho()), "solver"));
						cond++;
						solver_chosen = true;
					}
				} else if (activity.getAdded().equalsIgnoreCase("verified")) {
					if (!reviewer_chosen) {
						developerList.add(new Pair<User, String>(new User(activity.getWho()), "reviewer"));
						cond++;
						reviewer_chosen = true;
					}
				}
			}
		}
		return developerList;
	}

	/*
	 * Funkcja zwracajaca dane błedu
	 */
	public Bug getBug(int bugId) {
		User assigner = null;
		User solver = null;
		User reviewer = null;
		String desc = getBugDescription(bugId);

		// Tworzenie listy z programistami powiązanymi z błędem
		ArrayList<Pair<User, String>> developerList = getDevelopersConnectedToBug(getBugActivity(bugId));
		for (Pair<User, String> d : developerList) {
			if (d.getSecond().equals("assigner")) {
				assigner = d.getFirst();
			} else if (d.getSecond().equals("solver")) {
				solver = d.getFirst();
			} else {
				reviewer = d.getFirst();
			}
		}

		// Tworzenie listy termów
		TokenStream tokenStream = null;
		ArrayList<String> terms = new ArrayList<String>();
		try {
			Analyzer analyzer = new StopAnalyzer();
			tokenStream = analyzer.tokenStream("contents", new StringReader(desc));
			tokenStream = new StopFilter(tokenStream, stopSet);
			// OffsetAttribute offsetAttribute =
			// tokenStream.addAttribute(OffsetAttribute.class);
			CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);

			tokenStream.reset();
			while (tokenStream.incrementToken()) {
				// int startOffset = offsetAttribute.startOffset();
				// int endOffset = offsetAttribute.endOffset();
				String term = charTermAttribute.toString();
				terms.add(term);
			}
			analyzer.close();
			terms.sort(new Comparator<String>() {
				public int compare(String p1, String p2) {
					return p1.compareTo(p2);
				}
			});
		} catch (IOException e) {
			System.err.println("IOException happened while getting data about bug " + bugId + ".");
			e.printStackTrace();
		}
		return new Bug(bugId, assigner, solver, reviewer, terms);
	}

	/*
	 * Tworzy instancje potrzebne przy generowaniu modelu
	 */
	private InstanceList generateInstances() {
		ArrayList<Pipe> pipeList = new ArrayList<Pipe>();

		pipeList.add(new CharSequenceLowercase());
		pipeList.add(new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));
		pipeList.add(new TokenSequenceRemoveStopwords(new File("en.txt"), "UTF-8", false, false, false));
		pipeList.add(new TokenSequence2FeatureSequence());

		InstanceList instances = new InstanceList(new SerialPipes(pipeList));

		return instances;
	}

	/*
	 * Funkcja tworząca topic model ze słów w projekcie filename - plik z
	 * termami numTopics - liczba kategorii
	 */
	public ParallelTopicModel generateTopicModel(String filename, int numTopics) {
		ParallelTopicModel model = null;
		try {
			InstanceList instances = generateInstances();

			Reader fileReader = new InputStreamReader(new FileInputStream(new File(filename)), "UTF-8");
			instances.addThruPipe(
					new CsvIterator(fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"), 3, 2, 1));

			// alpha_t = 0.01, beta_w = 0.01
			model = new ParallelTopicModel(numTopics, 1.0, 0.01);
			model.addInstances(instances);
			model.setNumThreads(2);

			// 20 tys instancji
			model.setNumIterations(20000);
			model.estimate();
		} catch (IOException e) {
			System.err.println("IOException happened while generating topics model.");
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			System.err.println("UnsupportedOperationException happened while generating topics model.");
			e.printStackTrace();
		}
		return model;
	}

	/*
	 * Funkcja zwracająca i-tą kategorię w modelu
	 */
	public Topic getTopicFromTopicsModel(ParallelTopicModel model, int numTopic, InstanceList instances) {
		ArrayList<Pair<String, Double>> topicWords = new ArrayList<Pair<String, Double>>();
		Alphabet dataAlphabet = instances.getDataAlphabet();
		ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();

		Iterator<IDSorter> iterator = topicSortedWords.get(numTopic).iterator();
		if (iterator == null)
			return null;
		while (iterator.hasNext()) {
			IDSorter idCountPair = iterator.next();
			topicWords.add(new Pair<String, Double>(dataAlphabet.lookupObject(idCountPair.getID()).toString(),
					idCountPair.getWeight()));
		}
		topicWords.sort(new Comparator<Pair<String, Double>>() {
			public int compare(Pair<String, Double> p1, Pair<String, Double> p2) {
				return p1.getFirst().compareTo(p2.getFirst());
			}
		});
		return new Topic(numTopic, topicWords);
	}

	/*
	 * Funkcja zwracająca listę kategorii w modelu
	 */
	public ArrayList<Topic> getTopicsFromTopicsModel(int numTopics, String filename, String filenameDesc) {
		ArrayList<Topic> topicList = new ArrayList<Topic>();

		InstanceList instances = generateInstances();
		ParallelTopicModel model = generateTopicModel(filenameDesc, numTopics);

		FileWriter f1 = null;

		try {
			if (filename != null)
				f1 = new FileWriter(filename);

			int i = 0;
			Topic topic;
			while (i < numTopics) {
				topic = getTopicFromTopicsModel(model, i, instances);
				topicList.add(topic);
				if (filename != null)
					f1.write(topic.toString() + "\n");
				i++;
			}

			if (filename != null)
				f1.close();
		} catch (IOException e) {
			System.err.println("IOException happened while getting list with topics.");
			e.printStackTrace();
		}
		return topicList;
	}

	/*
	 * Funkcja zwracająca listę kategorii z pliku
	 */
	public ArrayList<Topic> getTopicsFromFile(String filename) {
		ArrayList<Topic> topicList = new ArrayList<Topic>();
		FileReader f1 = null;

		try {
			f1 = new FileReader(filename);
			BufferedReader br = new BufferedReader(f1);

			String line;
			String[] parts;
			while ((line = br.readLine()) != null) {
				int id;
				ArrayList<Pair<String, Double>> topicWords = new ArrayList<Pair<String, Double>>();
				String word;
				Double weight;
				Topic topic;

				parts = line.split(" ");
				id = Integer.parseInt(parts[0]);
				for (int i = 1; i < parts.length; i++) {
					if (i < parts.length - 1) {
						word = parts[i];
						i++;
						weight = Double.parseDouble(parts[i]);
						topicWords.add(new Pair<String, Double>(word, weight));
					}
				}
				topic = new Topic(id, topicWords);
				topicList.add(topic);
			}

			f1.close();
		} catch (IOException e) {
			System.err.println("IOException happened while getting list with topics.");
			e.printStackTrace();
		}
		return topicList;
	}

	/*
	 * Funkcja zwracająca listę z zamkniętymi błędami status - status błędu
	 * oznaczjący w konkretnym projektcie zamknięte błędy
	 */
	public ArrayList<Bug> getSolvedBugs(String status, String filename, String filenameTime, String filenameDesc) {
		ArrayList<Bug> solvedBugs = new ArrayList<Bug>();
		FileWriter f1 = null;
		FileWriter f2 = null;
		FileWriter f3 = null;
		try {
			allowWithoutCheckingCerts();

			if (filename != null && filenameTime != null && filenameDesc != null) {
				f1 = new FileWriter(filename);
				f2 = new FileWriter(filenameTime);
				f3 = new FileWriter(filenameDesc);
			}

			String descUrl = url + "/buglist.cgi?bug_status=" + status.toUpperCase()
					+ "&columnlist=bug_id&limit=0&order=bug_id&query_based_on=&query_format=advanced";
			URL realUrl = new URL(descUrl);

			BufferedReader in = new BufferedReader(new InputStreamReader(realUrl.openStream()));
			String inputLine;
			String result = "";
			while ((inputLine = in.readLine()) != null) {
				result = result + inputLine + "\n";
			}
			in.close();

			Document doc = Jsoup.parse(result); // Sparsowanie html
			Elements elems = doc.select("input[name=id]");

			int i = 0;
			if (filename != null && filenameTime != null && filenameDesc != null) 
				f2.write("GENERATING LIST OF SOLVED BUGS: " + "\n");
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date dateStart = new Date();
			if (filename != null && filenameTime != null && filenameDesc != null) 
				f2.write(dateFormat.format(dateStart).toString() + "\n");
			for (Element elem : elems) {
				if (elem.val().matches("\\d+")) {
					System.out.println(i + " " + Integer.parseInt(elem.val()));
					Bug bug = getBug(Integer.parseInt(elem.val()));
					if (bug.getTerms().size() > 0) {
						solvedBugs.add(bug);
						// f1.write("ID: " + bug.getId() + " ASSIGNER: " +
						// (bug.getAssigner() == null ? "NULL" :
						// bug.getAssigner().toString()) +
						// " SOLVER: " + (bug.getSolver() == null ? "NULL" :
						// bug.getSolver().toString()) +
						// " REVIEWER: " + (bug.getReviewer() == null ? "NULL" :
						// bug.getReviewer().toString() + " DESC: " +
						// bug.getDesc()) + "\n\n");
						if (filename != null && filenameTime != null && filenameDesc != null) {
							f1.write(bug.getId() + " " + (bug.getAssigner() == null ? "NULL" : bug.getAssigner().toString())
								+ " " + (bug.getSolver() == null ? "NULL" : bug.getSolver().toString()) + " "
								+ (bug.getReviewer() == null ? "NULL" : bug.getReviewer().toString()) + " "
								+ bug.getTermsToString() + "\n");
							f3.write(bug.getTermsToString() + "\n");
						}
					}
				}
				i++;
			}
			Date dateEnd = new Date();
			if (filename != null && filenameTime != null && filenameDesc != null) 
				f2.write(dateFormat.format(dateEnd).toString() + "\n");

			if (filename != null && filenameTime != null && filenameDesc != null) {
				f1.close();
				f2.close();
				f3.close();
			}

		} catch (MalformedURLException e) {
			System.err.println("MalformedURLException happened while getting list with solved bugs.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IOException happened while getting list with solved bugs.");
			e.printStackTrace();
		}
		return solvedBugs;
	}

	/*
	 * Funkcja zwracająca listę z zamkniętymi błędami status - status błędu
	 * oznaczjący w konkretnym projektcie zamknięte błędy
	 */
	public ArrayList<Bug> getSolvedBugsFromFile(String filename) {
		ArrayList<Bug> solvedBugs = new ArrayList<Bug>();
		FileReader f1 = null;

		try {

			f1 = new FileReader(filename);
			BufferedReader br = new BufferedReader(f1);

			String line;
			String[] parts;
			int j = 0;
			while ((line = br.readLine()) != null) {
				int id;
				User assigner;
				User solver;
				User reviewer;
				ArrayList<String> terms = new ArrayList<String>();

				parts = line.split(" ");
				id = Integer.parseInt(parts[0]);
				if (parts[1].equals("NULL"))
					assigner = null;
				else
					assigner = new User(parts[1]);
				if (parts[2].equals("NULL"))
					solver = null;
				else
					solver = new User(parts[2]);
				if (parts[3].equals("NULL"))
					reviewer = null;
				else
					reviewer = new User(parts[3]);
				for (int i = 4; i < parts.length; i++)
					terms.add(parts[i]);
				System.out.println(j);
				solvedBugs.add(new Bug(id, assigner, solver, reviewer, terms));
				j++;
			}

			f1.close();
		} catch (IOException e) {
			System.err.println("IOException happened while getting list with topics.");
			e.printStackTrace();
		}
		return solvedBugs;
	}

	/*
	 * Towrzy plik z termami z repozytorium
	 */
	public void makeFileWithTerms(ArrayList<Bug> bugs, String filename) {
		FileWriter f1 = null;

		try {
			f1 = new FileWriter(filename);
			for (Bug bug : bugs)
				f1.write(bug.getTermsToString() + "\n");

			f1.close();
		} catch (IOException e) {
			System.err.println("IOException happened while making file with terms.");
			e.printStackTrace();
		}
	}

	/*
	 * Funkcja tworzy zbiór użytkowników w projekcie
	 */
	public ArrayList<User> getUsers(ArrayList<Bug> bugList, String filename) {
		FileWriter f1 = null;
		Set<User> users = new TreeSet<User>();
		ArrayList<User> userList = new ArrayList<User>();

		int i = 0;
		for (Bug bug : bugList) {
			System.out.println(i);
			if (bug.getAssigner() != null)
				users.add(bug.getAssigner());
			if (bug.getSolver() != null)
				users.add(bug.getSolver());
			if (bug.getReviewer() != null)
				users.add(bug.getReviewer());
			i++;
		}

		try {
			if (filename != null) {
				f1 = new FileWriter(filename);
				i = 0;
				for (User user : users) {
					System.out.println(i);
					f1.write(user.getName() + "\n");
					i++;
				}
				f1.close();
			}
		} catch (IOException e) {
			System.err.println("IOException happened while getting list with users.");
			e.printStackTrace();
		}
		userList.addAll(users);
		return userList;
	}

	/*
	 * Funkcja tworzy zbiór użytkowników w projekcie z pliku
	 */
	public ArrayList<User> getUsersFromFile(String filename) {
		Set<User> users = new HashSet<User>();
		ArrayList<User> userList = new ArrayList<User>();
		FileReader f1 = null;

		try {
			f1 = new FileReader(filename);
			BufferedReader br = new BufferedReader(f1);

			String line;
			while ((line = br.readLine()) != null)
				users.add(new User(line));
			f1.close();
		} catch (IOException e) {
			System.err.println("IOException happened while getting list with users.");
			e.printStackTrace();
		}
		userList.addAll(users);

		return userList;
	}

	/*
	 * Funkcja tworzy liczniki akcji
	 */
	public ArrayList<Integer> getActionsCounter(ArrayList<Bug> bugList, String filename) {
		FileWriter f1 = null;
		ArrayList<Integer> actionsCounter = new ArrayList<Integer>();
		int i = 0;
		int assigners = 0, solvers = 0, reviewers = 0;
		for (Bug bug : bugList) {
			System.out.println(i);
			if (bug.getAssigner() != null)
				assigners++;
			if (bug.getSolver() != null)
				solvers++;
			if (bug.getReviewer() != null)
				reviewers++;
			i++;
		}

		try {
			if (filename != null) {
				f1 = new FileWriter(filename);
				i = 0;
				f1.write(assigners + "\n");
				f1.write(solvers + "\n");
				f1.write(reviewers + "\n");
				f1.close();
			}
		} catch (IOException e) {
			System.err.println("IOException happened while getting counter of actions.");
			e.printStackTrace();
		}
		actionsCounter.add(assigners);
		actionsCounter.add(solvers);
		actionsCounter.add(reviewers);

		return actionsCounter;
	}

	/*
	 * Funkcja tworzy liczniki akcji z pliku
	 */
	public ArrayList<Integer> getActionsCounterFile(String filename) {
		ArrayList<Integer> actionsCounter = new ArrayList<Integer>();
		FileReader f1 = null;

		try {
			f1 = new FileReader(filename);
			BufferedReader br = new BufferedReader(f1);

			String line;
			while ((line = br.readLine()) != null)
				actionsCounter.add(Integer.parseInt(line));
			f1.close();
		} catch (IOException e) {
			System.err.println("IOException happened while getting counter of actions.");
			e.printStackTrace();
		}

		return actionsCounter;
	}

	/*
	 * Funkcja zwracająca liczbę powiązań między błędem, a kategorią
	 */
	private int getAssociationsNumberBugTopic(Bug bug, Topic topic) {
		boolean cond = true;
		int i = 0, j = 0;
		int len1 = bug.getTerms().size();
		int len2 = topic.getWords().size();

		int associations = 0;
		String term;
		String word;
		while (cond) {
			if ((i >= len1) || (j >= len2))
				break;
			term = bug.getTerms().get(i);
			word = topic.getWords().get(j).getFirst();
			if (term.equals(word)) {
				associations++;
				i++;
			} else if (term.compareTo(word) < 0)
				i++;
			else
				j++;
		}

		return associations;
	}

	/*
	 * Funkcja generująca macierz wiążącą raporty o błędach z kategoriami
	 */
	public BugsTopicsMatrix generateBugsTopicsMatrix(ArrayList<Bug> bugList, ArrayList<Topic> topicList,
			String filename) {
		int bugsNum = bugList.size();
		int topicsNum = topicList.size();
		BugsTopicsMatrix matrix = new BugsTopicsMatrix(bugsNum, topicsNum);
		FileWriter f1 = null;

		try {
			if (filename != null)
				f1 = new FileWriter(filename);

			int associations = 0;
			for (int i = 0; i < bugsNum; i++) {
				// System.out.println("A " + i);
				for (int j = 0; j < topicsNum; j++) {
					// System.out.println("B " + j);
					associations = getAssociationsNumberBugTopic(bugList.get(i), topicList.get(j));
					matrix.increaseFieldValue(i, j, associations);
					if (filename != null)
						f1.write(associations + " ");
				}
				if (filename != null)
					f1.write("\n");
			}
			if (filename != null)
				f1.close();
		} catch (IOException e) {
			System.err.println("IOException happened while creating matrix with Bugs Topics.");
		}

		return matrix;
	}

	/*
	 * Funkcja zwracająca liczbę powiązań między kategorią, a użytkownikiem
	 */
	double getAssociationsNumberTopicUser(BugsTopicsMatrix btmatrix, int topicNum, User user, ArrayList<Bug> bugList,
			int assigners, int solvers, int reviewers) {
		double assoctiations = 0;

		Bug bug;
		double assignersProc = assigners * 100 / (assigners + solvers + reviewers);
		double solversProc = solvers * 100 / (assigners + solvers + reviewers);
		double reviewersProc = reviewers * 100 / (assigners + solvers + reviewers);
		for (int i = 0; i < bugList.size(); i++) {
			bug = bugList.get(i);
			if (bug.getAssigner() != null && bug.getAssigner().getName().equals(user.getName()))
				assoctiations += btmatrix.getFieldValue(i, topicNum) * assignersProc;

			if (bug.getSolver() != null && bug.getSolver().getName().equals(user.getName()))
				assoctiations += btmatrix.getFieldValue(i, topicNum) * solversProc;

			if (bug.getReviewer() != null && bug.getReviewer().getName().equals(user.getName()))
				assoctiations += btmatrix.getFieldValue(i, topicNum) * reviewersProc;
		}
		return assoctiations;
	}

	/*
	 * Funkcja generująca macierz wiążącą użytkowników z kategoriami
	 */
	public TopicsUsersMatrix generateTopicsUsersMatrix(BugsTopicsMatrix btmatrix, int topicNum,
			ArrayList<User> userList, ArrayList<Bug> bugList, int assigners, int solvers, int reviewers,
			String filename) {
		int usersNum = userList.size();
		TopicsUsersMatrix matrix = new TopicsUsersMatrix(topicNum, usersNum);
		FileWriter f1 = null;
		double associations = 0;

		try {
			if (filename != null)
				f1 = new FileWriter(filename);

			for (int i = 0; i < topicNum; i++) {
				for (int j = 0; j < usersNum; j++) {
					associations = getAssociationsNumberTopicUser(btmatrix, i, userList.get(j), bugList, assigners,
							solvers, reviewers);
					matrix.increaseFieldValue(i, j, associations);
					f1.write(associations + " ");
				}
				f1.write("\n");
			}

			if (filename != null)
				f1.close();
		} catch (IOException e) {
			System.err.println("IOException happened while creating matrix with Topics Users.");
		}
		return matrix;
	}

	/*
	 * Funckacja znajdująca znaczące kategorie
	 */
	private ArrayList<Integer> getSignificiantTopicNumbers(BugsTopicsMatrix matrix, int significiantTopicNum,
			String filename) {
		ArrayList<Pair<Integer, Integer>> topicsCount = new ArrayList<Pair<Integer, Integer>>(); // tablica z parami kategoria, liczba powiązań

		FileWriter f1 = null;
		int associations;
		for (int i = 0; i < matrix.getTopicsNum(); i++) {
			associations = 0;
			for (int j = 0; j < matrix.getBugsNum(); j++) {
				associations += matrix.getFieldValue(j, i);
			}
			topicsCount.add(new Pair<Integer, Integer>(i, associations));
		}
		topicsCount.sort(new Comparator<Pair<Integer, Integer>>() {
			public int compare(Pair<Integer, Integer> p1, Pair<Integer, Integer> p2) {
				return p1.getSecond().compareTo(p2.getSecond());
			}
		});

		ArrayList<Integer> significiantTopicList = new ArrayList<Integer>();
		try {
			if (filename != null)
				f1 = new FileWriter(filename);
			for (int i = 0; i < significiantTopicNum; i++) {
				if (i >= topicsCount.size())
					break;
				if (filename != null)
					f1.write(topicsCount.get(i).getFirst() + "\n");
				significiantTopicList.add(topicsCount.get(i).getFirst());
			}
			if (filename != null)
				f1.close();
		} catch (IOException e) {
			System.err.println("IOException happened while creating matrix with Bugs Topics.");
		}
		return significiantTopicList;
	}

	/*
	 * Funkcja tworząca listę użytkowników do rozwiązania błędu
	 */
	public ArrayList<User> generateUsersListForBug(TopicsUsersMatrix matrix, int usersNum, ArrayList<User> userList,
			ArrayList<Topic> topicList, int signiNum, int bugNr, String filename) {
		ArrayList<Pair<Integer, Double>> topicsCount = new ArrayList<Pair<Integer, Double>>(); 																		
		Bug bug = getBug(bugNr);
		ArrayList<Bug> bugs = new ArrayList<Bug>();
		bugs.add(bug);

		BugsTopicsMatrix bugsTopicsMatrix = generateBugsTopicsMatrix(bugs, topicList, null);
		ArrayList<Integer> significiantTopicsNum = getSignificiantTopicNumbers(bugsTopicsMatrix, signiNum, null);

		double associations;
		for (int i = 0; i < matrix.getTopicsNum(); i++) {
			if (!significiantTopicsNum.contains(i))
				continue;
			associations = 0;
			for (int j = 0; j < matrix.getUsersNum(); j++) {
				associations += matrix.getFieldValue(i, j);
			}
			topicsCount.add(new Pair<Integer, Double>(i, associations));
		}
		topicsCount.sort(new Comparator<Pair<Integer, Double>>() {
			public int compare(Pair<Integer, Double> p1, Pair<Integer, Double> p2) {
				return p1.getSecond().compareTo(p2.getSecond());
			}
		});

		ArrayList<User> usersForBugs = new ArrayList<User>();
		FileWriter f1 = null;
		try {
			if (filename != null)
				f1 = new FileWriter(filename);
			for (int i = 0; i < usersNum; i++) {
				if (i >= topicsCount.size())
					break;
				usersForBugs.add(userList.get(topicsCount.get(i).getFirst()));
				f1.write(userList.get(topicsCount.get(i).getFirst()).getName() + "\n");
			}
			if (filename != null)
				f1.close();
		} catch (IOException e) {
			System.err.println("IOException happened while creating list of users for bug.");
		}
		return usersForBugs;
	}

	/*
	 * Końcowa funkcja
	 */
	public ArrayList<User> generateList(int bugNr, String status, int topicNum, int signiNum, int numTopics,
			String filename, String filenameTopicsUsers, String filenameBugsTopics, String filenameActionsCounter,
			String filenameBugs1, String filenameBugs2, String filenameDesc, String filenameUsers, String filenameModel) {
		ArrayList<Bug> bugList = getSolvedBugs(status, filenameBugs1, filenameBugs2, filenameDesc);
		ArrayList<Topic> topicList = getTopicsFromTopicsModel(numTopics, filenameModel, filenameDesc);
		ArrayList<User> userList = getUsers(bugList, filenameUsers);
		ArrayList<Integer> actionsCounter = getActionsCounter(bugList, filenameActionsCounter);
		BugsTopicsMatrix btmatrix = generateBugsTopicsMatrix(bugList, topicList, filenameBugsTopics);
		TopicsUsersMatrix tumatrix = generateTopicsUsersMatrix(btmatrix, topicNum, userList, bugList,
				actionsCounter.get(0), actionsCounter.get(1), actionsCounter.get(2), filenameTopicsUsers);
		ArrayList<User> usersList = generateUsersListForBug(tumatrix, userList.size(), userList, topicList, signiNum,
				bugNr, filename);
		return usersList;
	}

}
