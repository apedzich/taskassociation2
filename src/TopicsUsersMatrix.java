/*
 * Klasa odpowiadająca macierzy wiążącej kategorię z programistą
 */
public class TopicsUsersMatrix {

	/*
	 * Licbza kategorii
	 */
	final private int topicsNum;
	/*
	 * Liczba programistów
	 */
	final private int usersNum;
	/*
	 * Macierz
	 */
	private double[][] matrix;
			
	TopicsUsersMatrix(int topicsNum, int usersNum) {
		this.topicsNum = topicsNum;
		this.usersNum = usersNum;
		this.matrix = new double[topicsNum][usersNum];
	}
	
	public int getTopicsNum() {
		return topicsNum;
	}
			
	public int getUsersNum() {
		return usersNum;
	}

	public double getFieldValue(int topicNr, int userNr) {
		return matrix[topicNr][userNr];
	}
			
	public void setFieldValue(int topicNr, int userNr, double value) {
		matrix[topicNr][userNr] = value;
	}
			
	public void increaseFieldValue(int topicNr, int userNr, double inc) {
		matrix[topicNr][userNr] += inc;
	}

}
