/*
 * Klasa odpowiadająca użytkownikowi repozytorium
 */
public class User implements Comparable<User> {
	
	/*
	 * Nazwa użytkownika
	 */
	private final String name;
    
    public User(String name) {
    	this.name = name;
    }
    
    public String getName() {
    	return name;
    }
    
    /*
     * Sprawdza czy użytkownik jest aktywny
     */
    public Boolean isActive() {
    	return (!name.contains("(gone)") && !name.contains("(inactive)") && !name.contains("(fired)"));
    }
    
    @Override
    public String toString() {
    	return name;
    }
    
    @Override 
    public int compareTo(User user) {
    	return this.name.compareTo(user.getName());
    }
}